package com.houlihan.sean;

public class Donation {

    private final String name;
    private final int quantity;
    private final double value;
    private final boolean is_new;


    public Donation(String name, int quantity, boolean is_new, double value) {
        this.name = name;
        this.quantity = quantity;
        this.is_new = is_new;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public boolean is_new() {
        return is_new;
    }

    public double getValue() {
        return value;
    }
}

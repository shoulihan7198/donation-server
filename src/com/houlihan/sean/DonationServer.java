package com.houlihan.sean;


import java.io.IOException;
import java.net.ServerSocket;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class DonationServer {

    private ServerSocket server;
    private final static String VERSION = "1.0.3";
    private static boolean verbose = false;
    final private Scanner scanner = new Scanner(System.in);


    public static void setVerbose(boolean verbose) {
        DonationServer.verbose = verbose;
    }

    public static boolean isVerbose() {
        return DonationServer.verbose;
    }


    DonationServer(int port) {
        try {
            Utils.printColor("Creating server listening on port " + port + " " + Utils.getCurrentTime(), Utils.ANSI_GREEN);
            if (verbose) Utils.printColor("Verbose logging is enabled", Utils.ANSI_GREEN);
            server = new ServerSocket(port);
            Thread serverThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        while (true) {
                            new SocketThread(server.accept()).run();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });

            Thread consoleThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        String input = scanner.next();
                        switch (input) {
                            case "exit":
                                Utils.printColor("Exiting safely...", Utils.ANSI_GREEN);
                                System.exit(0);
                                break;
                            case "help":
                                Utils.printColor("Commands:", Utils.ANSI_GREEN);
                                Utils.printColor("help     Displays this help information", Utils.ANSI_GREEN);
                                Utils.printColor("exit     Exits the server", Utils.ANSI_GREEN);
                                Utils.printColor("info     Displays information about the server", Utils.ANSI_GREEN);
                                System.out.println();
                                Utils.printColor("Arguments:", Utils.ANSI_GREEN);
                                Utils.printColor("-v, -verbose     Starts the server with verbose output", Utils.ANSI_GREEN);
                                Utils.printColor("-p, -port INT    Starts the server listening on selected port", Utils.ANSI_GREEN);
                                break;
                            case "info":
                                Utils.printColor("Info:", Utils.ANSI_GREEN);
                                Utils.printColor("Version:     " + VERSION, Utils.ANSI_GREEN);
                                Utils.printColor("Author:      Sean Houlihan", Utils.ANSI_GREEN);
                                break;


                        }
                    }
                }
            });
            consoleThread.start();
            serverThread.start();

        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    public static void main(String[] args) {

        DonationServer donationServer;
        try {

            List<String> arguments = Arrays.asList(args);

            Integer port = null;
            for (String arg : arguments) {

                if (arg.equals("-p") || arg.equals("-port")) {
                    int art_port = Integer.parseInt(arguments.get(arguments.indexOf(arg) + 1));
                    if (art_port > 0 && art_port < 66535) port = art_port;
                    else {
                        Utils.printColor("Invalid port, starting anywways with default port", Utils.ANSI_GREEN);
                    }


                } else if (arg.equals("-v") || arg.equals("-verbose")) {
                    DonationServer.setVerbose(true);
                }
            }

            if (port != null) donationServer = new DonationServer(port);
            else donationServer = new DonationServer(5000);


        } catch (ArrayIndexOutOfBoundsException as) {
            donationServer = new DonationServer(5000);
        } catch (Exception e) {

        }
    }
}




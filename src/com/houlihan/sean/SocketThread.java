package com.houlihan.sean;

import com.google.gson.Gson;

import java.io.*;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Sean Houlihan for Habitat for Humanity
 */
class SocketThread implements Runnable {

    public static final String MODE_INSERT_NEW = "0";
    public static final String MODE_REQUEST_ID = "-1";
    public static final String MODE_INSERT_EXISTING = "-2";
    public static final String RESULT_SUCCESS = "-3";
    public static final String RESULT_FAILED = "-4";
    public static final String RESULT_NOT_FOUND = "-5";
    private Connection mySqlConnection;
    private DonationForm donationForm;
    private BufferedReader reader;
    private BufferedWriter out;
    private Socket clientSocket;


    public SocketThread(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {

        try {
            if (DonationServer.isVerbose())
                Utils.printColor("Client connected, " + Utils.getCurrentTime(), Utils.ANSI_CYAN);
            clientSocket.setSoTimeout(8000);
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

            //read mode client wants to start in
            String mode = reader.readLine();
            if (mode.equals(MODE_REQUEST_ID) || mode.equals(MODE_INSERT_NEW) || mode.equals(MODE_INSERT_EXISTING)) {

                //start connection for use later
                mySqlConnection = DriverManager.getConnection("jdbc:mysql://testdb.crmjxqwzp9ps.us-west-2.rds.amazonaws.com:3306/restore_donationsdb", "android_client", "9wqzPkYn");

                if (mode.equals(MODE_REQUEST_ID)) requestID();
                else {
                    donationForm = new Gson().fromJson(reader.readLine(), DonationForm.class);
                    if (donationForm != null) {
                        //Data exists

                        if (DonationServer.isVerbose()) {
                            Utils.printColor("Donator info:\n" + donationForm.getFirstName() + "\n" + donationForm.getLastName() + "\n" +
                                    donationForm.getAddress() + "\n" + donationForm.getCity() + "\n" + donationForm.getState() +
                                    "\n" + donationForm.getZipCode() + "\n" + donationForm.getHomePhone() + "\n" + donationForm.getCellPhone() + "\n" + donationForm.getEmail(), Utils.ANSI_CYAN);

                            for (Donation donation : donationForm.getDonations()) {
                                Utils.printColor("Donated items:\n" + donation.getName() + " " + donation.getQuantity(), Utils.ANSI_CYAN);
                            }
                        }


                        if (mode.equals(MODE_INSERT_NEW)) insertNew();
                        else if (mode.equals(MODE_INSERT_EXISTING)) insertExisting();

                        //commit changes to database if autocommit disabled
                        if (!mySqlConnection.getAutoCommit()) mySqlConnection.commit();

                    } else {

                        Utils.printColor("DonationForm from client is null " + Utils.getCurrentTime(), Utils.ANSI_RED);
                        sendToClient(RESULT_FAILED);
                    }
                }

            } else {

                Utils.printColor("Mode not recognized: " + mode + ", " + Utils.getCurrentTime(), Utils.ANSI_RED);
                sendToClient(RESULT_FAILED);
            }


        } catch (IOException | SQLException st) {
            Utils.printColor(st.getMessage() + " at " + Utils.getCurrentTime(), Utils.ANSI_RED);
            if (DonationServer.isVerbose()) {
                st.printStackTrace();
            }
        } catch (Exception e) {
            sendToClient(RESULT_FAILED);
            e.printStackTrace();

        } finally {

            try {

                reader.close();
                out.close();
                mySqlConnection.close();
                clientSocket.close();

            } catch (Exception e) {
                Utils.printColor("Exception occurred closing objects before releasing socket", Utils.ANSI_RED);
                if (DonationServer.isVerbose()) {
                    e.printStackTrace();
                }
            }

        }

    }

    private void insertNew() throws SQLException, IOException {

        String personID = checkAlreadyExists(donationForm.getEmail());

        if (personID.isEmpty()) {

            //user is unique

            String homePhone = (donationForm.getHomePhone().isEmpty()) ? "NULL" : "'" + donationForm.getHomePhone() + "'";
            String cellPhone = (donationForm.getCellPhone().isEmpty()) ? "NULL" : "'" + donationForm.getCellPhone() + "'";
            String donators_statement = "INSERT INTO donators VALUES(NULL, '" + donationForm.getFirstName() + "', '" + donationForm.getLastName() + "', '" + donationForm.getAddress() +
                    "', '" + donationForm.getCity() + "', '" + donationForm.getState() + "', '" + donationForm.getZipCode() + "', " + homePhone +
                    ", " + cellPhone + ", '" + donationForm.getEmail() + "');";
            String donation_statement = "INSERT INTO donations VALUES(NULL, NOW(), LAST_INSERT_ID(), " + donationForm.getRestoreZipCode() + ");";
            mySqlConnection.createStatement().executeUpdate(donators_statement);
            mySqlConnection.createStatement().executeUpdate(donation_statement);
            addDonationItems();
            sendToClient(RESULT_SUCCESS);

        } else {

            //user submitted new form even though their email already exists in database,
            //silently updating existing their existing record instead of creating new one

            donationForm.setPersonID(personID);
            insertExisting();
        }

    }

    private void requestID() throws IOException {

        String personID = checkAlreadyExists(reader.readLine());
        if (personID == null || personID.isEmpty()) sendToClient(RESULT_NOT_FOUND);
        else sendToClient(personID);

    }

    private void insertExisting() throws SQLException, IOException {

        //record of user already exists, update the existing record with this donation

        String update = "INSERT INTO donations VALUES(NULL, NOW(), " + donationForm.getPersonID() + ", " + donationForm.getRestoreZipCode() + ");";
        mySqlConnection.createStatement().executeUpdate(update);
        String getExisitingData = "SELECT donator_id FROM donations WHERE donation_id=LAST_INSERT_ID()";
        ResultSet resultSet = mySqlConnection.createStatement().executeQuery(getExisitingData);
        resultSet.next();
        String donator_id = String.valueOf(resultSet.getInt(1));
        String getDonator = "SELECT * FROM donators WHERE donator_id='" + donator_id + "'";
        ResultSet resultSet1 = mySqlConnection.createStatement().executeQuery(getDonator);
        resultSet1.next();
        donationForm.setFirstName(resultSet1.getString("first"));
        donationForm.setLastName(resultSet1.getString("last"));
        addDonationItems();
        sendToClient(RESULT_SUCCESS);

    }


    private String checkAlreadyExists(String email) {

        String statement = "SELECT donator_id FROM donators WHERE email='" + email + "'";
        String id = "";
        try {
            ResultSet resultSet = mySqlConnection.createStatement().executeQuery(statement);
            resultSet.next();
            id = String.valueOf(resultSet.getInt(1));
        } catch (SQLException s) {
            //Email not found in database
        }
        return id;
    }

    private void addDonationItems() throws SQLException, IOException {
        String last_row = "SELECT donation_id FROM donations WHERE donation_id=LAST_INSERT_ID()";
        ResultSet resultSet = mySqlConnection.createStatement().executeQuery(last_row);
        resultSet.next();
        int last = resultSet.getInt(1);
        if (last > 0) {
            for (Donation donation : donationForm.getDonations()) {
                String condition = (donation.is_new()) ? "1" : "0";
                mySqlConnection.createStatement().executeUpdate("INSERT INTO donation_items VALUES(NULL, '" + donation.getName() + "', " + donation.getQuantity() + ", " + last + ", '" + condition + "', " + donation.getValue() + ");");
            }
            savePicture(String.valueOf(last));
            DonationAutoEmailer.sendEmail(donationForm.getEmail(), donationForm, String.valueOf(last));
        } else {
            throw new SQLException("donator_id not valid");
        }
    }

    private void savePicture(String donationID) throws IOException {

        File myFile = new File("signatures/" + donationID + ".webp");
        FileOutputStream fos = new FileOutputStream(myFile);
        fos.write(donationForm.getSignaturePNG());
        fos.flush();
        fos.close();

    }


    private void sendToClient(String result) {
        try {
            out.write(result + "\n");
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
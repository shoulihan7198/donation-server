package com.houlihan.sean;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;


public class DonationAutoEmailer {

    private static final String emailSubject = "Thank you for your donation";
    private static final String emailFrom = "restoredonapp@habitatsoco.org";
    private static final String password = "RDApp!7707";
    private static Properties props;
    private static Session session;

    static {

        props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailFrom, password);
            }
        });
    }

    public static void sendEmail(String to, DonationForm donationForm, String donationID) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    String rowHTML = "";
                    for (int i = 0; i < donationForm.getDonations().size(); i++) {

                        Donation donation = donationForm.getDonations().get(i);
                        String condition = (donation.is_new()) ? "New" : "Used";
                        String className = (donationForm.getDonations().indexOf(donation) == (donationForm.getDonations().size() - 1)) ? "item" : "item last";
                        String row = "<tr class=" + className + "><td style=\"padding:5px;vertical-align:top;border-bottom:1px solid #eee\">" + donation.getName() + "<td style=\"padding:5px;vertical-align:top;text-align:right;border-bottom:1px solid #eee\">" + donation.getQuantity() + "<td style=\"padding:5px;vertical-align:top;text-align:right;border-bottom:1px solid #eee\">" + "$" + donation.getValue() + "<td style=\"padding:5px;vertical-align:top;text-align:right;border-bottom:1px solid #eee\">" + condition;
                        rowHTML = rowHTML.concat(row);
                    }

                    String html = "<!DOCTYPE html><meta charset=utf-8><title>Your ReStore Receipt</title><style>.invoice-box{max-width:800px;margin:auto;padding:30px;border:1px solid #eee;box-shadow:0 0 10px rgba(0,0,0,.15);font-size:16px;line-height:24px;font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;color:#555}.invoice-box table{width:100%;line-height:inherit;text-align:left}.invoice-box table td{padding:5px;vertical-align:top}.invoice-box table tr td:nth-child(2){text-align:right}.invoice-box table tr td:nth-child(3){text-align:right}.invoice-box table tr td:nth-child(4){text-align:right}.invoice-box table tr.top table td{padding-bottom:20px}.invoice-box table tr.top table td.title{font-size:45px;line-height:45px;color:#333}.invoice-box table tr.information table td{padding-bottom:40px}.invoice-box table tr.heading td{background:#eee;border-bottom:1px solid #ddd;font-weight:700}.invoice-box table tr.details td{padding-bottom:20px}.invoice-box table tr.item td{border-bottom:1px solid #eee}.invoice-box table tr.item.last td{border-bottom:none}.invoice-box table tr.total td:nth-child(2){border-top:2px solid #eee;font-weight:700}@media only screen and (max-width:600px){.invoice-box table tr.top table td{width:100%;display:block;text-align:center}.invoice-box table tr.information table td{width:100%;display:block;text-align:center}}</style><div class=invoice-box style=\"max-width:800px;margin:auto;padding:30px;border:1px solid #eee;box-shadow:0 0 10px rgba(0,0,0,.15);font-size:16px;line-height:24px;font-family:'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif;color:#555\"><table style=width:100%;line-height:inherit;text-align:left cellpadding=0 cellspacing=0><tr class=top><td style=padding:5px;vertical-align:top colspan=4><table style=width:100%;line-height:inherit;text-align:left><tr><td style=padding:5px;vertical-align:top;padding-bottom:20px;font-size:45px;line-height:45px;color:#333 class=title><img src=http://www.evansvillehabitat.org/images/common/logo.png style=width:100%;max-width:300px><td style=padding:5px;vertical-align:top;text-align:right;padding-bottom:20px>Donation id: " + donationID + "<br>Date: " + Utils.getReceiptTime() + "<br></table><tr class=information><td style=padding:5px;vertical-align:top colspan=4><table style=width:100%;line-height:inherit;text-align:left><tr><td style=padding:5px;vertical-align:top;padding-bottom:40px>Habitat For Humanity ReStore<br>1201 Piner Rd #500<br>Santa Rosa, CA 95403<td style=padding:5px;vertical-align:top;text-align:right;padding-bottom:40px>" + donationForm.getFirstName() + " " + donationForm.getLastName() + "<br>" + donationForm.getEmail() + "</table><tr class=heading><td style=\"padding:5px;vertical-align:top;background:#eee;border-bottom:1px solid #ddd;font-weight:700\">Item<td style=\"padding:5px;vertical-align:top;text-align:right;background:#eee;border-bottom:1px solid #ddd;font-weight:700\">Quantity<td style=\"padding:5px;vertical-align:top;text-align:right;background:#eee;border-bottom:1px solid #ddd;font-weight:700\">Estimated Value<td style=\"padding:5px;vertical-align:top;text-align:right;background:#eee;border-bottom:1px solid #ddd;font-weight:700\">Condition" + rowHTML + "</table></div>";
                    System.out.print("Starting...");
                    Message message = new MimeMessage(session);
                    message.setFrom(new InternetAddress(emailFrom));
                    message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
                    message.setSubject(emailSubject);
                    message.setContent(html, "text/html");
                    Transport.send(message);
                    System.out.println("Successfully sent message: ");

                } catch (MessagingException ms) {


                    if (DonationServer.isVerbose()) {
                        Utils.printColor("Sending receipt email failed!", Utils.ANSI_RED);
                        ms.printStackTrace();
                    }

                }

            }
        }).start();


    }

}

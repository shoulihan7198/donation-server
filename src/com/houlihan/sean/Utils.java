package com.houlihan.sean;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by Sean on 3/10/2016.
 */
public class Utils {

    private static final GregorianCalendar calendar = new GregorianCalendar(Locale.ENGLISH);
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static void printColor(String message, String textColor) {
        System.out.println(textColor + message + ANSI_RESET);
    }

    public static String getCurrentTime() {
        calendar.setTimeInMillis(System.currentTimeMillis());
        return calendar.getTime().toString();
    }

    public static String getReceiptTime() {
        calendar.setTimeInMillis(System.currentTimeMillis());
        SimpleDateFormat fmt = new SimpleDateFormat("dd MMMM, yyyy");
        fmt.setCalendar(calendar);
        return fmt.format(calendar.getTime());

    }


}

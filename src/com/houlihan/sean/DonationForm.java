package com.houlihan.sean;

import java.util.List;

public class DonationForm {

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private String firstName;
    private String lastName;
    private final String address;
    private final String city;
    private final String state;
    private final String zipCode;
    private final String homePhone;
    private final String cellPhone;
    private final String email;
    private final List<Donation> donations;
    private String personID;
    private byte[] signaturePNG;


    private final String restoreZipCode;

    DonationForm(String firstName, String lastName, String address,
                 String city, String state, String zipCode,
                 String homePhone, String cellPhone, String email,
                 List<Donation> donations, String restoreZipCode) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
        this.homePhone = homePhone;
        this.cellPhone = cellPhone;
        this.email = email;
        this.donations = donations;
        this.restoreZipCode = restoreZipCode;
    }
    public String getLastName() {
        return lastName;
    }

    public List<Donation> getDonations() {
        return donations;
    }

    public String getEmail() {
        return email;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getState() {
        return state;
    }

    public String getRestoreZipCode() {
        return restoreZipCode;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }

    public void setSignatureByteArray(byte[] array) {
        this.signaturePNG = array;
    }

    public byte[] getSignaturePNG() {
        return signaturePNG;
    }


}

